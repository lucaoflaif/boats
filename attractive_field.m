function [attractive_field] = attractive_field(gamma, arrive_point_angle, scanned_angles)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

theta_g = arrive_point_angle;

%theta_i is a vector containing the ith data label (x value)
% i=(1...361)
theta_i = scanned_angles;

attractive_field = gamma*abs(theta_g-theta_i);

end

