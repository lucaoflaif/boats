function [f_k] = likelihood_function(max_detection_range, obstacle_average_distance, sigma_k,center_of_obstacle,scanned_angles)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%theta_k id the value of the center angle's obstacle
theta_k = center_of_obstacle;

%theta_i is a vector containing the ith data label (x value)
% i=(1...361)
theta_i = scanned_angles;
%f_k = likelihood function
f_k = A_k(max_detection_range, obstacle_average_distance).*exp(-((theta_k-theta_i).^2)./(2.*(sigma_k.^2)));

end

