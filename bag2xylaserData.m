bagname = '2011-01-24-06-18-27.bag';

if not(exist('bag','var'))
    bag = rosbag(bagname); 
    bag2 = select(bag, 'Topic', '/base_scan');
    msgs = readMessages(bag2);
    
    bag3 = select(bag, 'MessageType', 'sensor_msgs/Image');
    msgsImg = readMessages(bag3);
end

for i = 1:size(msgs)
    ranges = msgs{i,1}.Ranges;
        for r = ranges
            if r < msgs{i,1}.RangeMin
                ranges(ranges==r)=[];
            end
            if r > msgs{i,1}.RangeMax
                ranges(ranges==r)=[];
            end
        end
  
    angles = [rad2deg(msgs{i,1}.AngleMin):rad2deg(msgs{i,1}.AngleIncrement):rad2deg(msgs{i,1}.AngleMax)];
    subplot(1,2,1);
    plot(angles, ranges);
    
    ranges = lowpass(double(ranges), 1e-2,'ImpulseResponse','iir');
    %ranges = invert_data(ranges);    
    subplot(1,2,2);
    plot(angles,ranges);
    %angles = [(msgs{i,1}.AngleMin):(msgs{i,1}.AngleIncrement):(msgs{i,1}.AngleMax)];    
    %s = lidarScan(ranges, angles);
    %plot(s);
    
    pause(0.005);
end
