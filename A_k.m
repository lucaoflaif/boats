function [A_k] = A_k(max_detection_range, obstacle_average_distance)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

%d_max is the maximum detection range of the range sensor
d_max = max_detection_range;

%d_k is the average distance to each obstacle
d_k = obstacle_average_distance;

d_k_ = d_max - d_k;

A_k = d_k_ * exp(1/2);
end

