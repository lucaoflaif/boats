function [sigma_k] = sigma_k(average_distance, average_angle, veichle_width)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

d = average_distance;
lower_phi = deg2rad(average_angle);
w = veichle_width;

upper_phi_k = 2*atan2(d*tan((lower_phi/2))+(w/2),d);

upper_phi_k =(upper_phi_k);

%since upper_phi_k = 2*sigma_k
sigma_k = rad2deg(upper_phi_k/2);
end

