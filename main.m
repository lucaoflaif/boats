VEICHLE_WIDTH = 0.5;
MAX_DETECTION_RANGE = 7;

x1 = [100:-1:0];
x2=[-1:-1:-100];
x = [x1 x2];
y = [linspace(8,8,10) linspace(8,8,15) linspace(8,6,5) linspace(6,2,20) linspace(2,3,25) linspace(8,9.5,45) linspace(2.5,1.2,60) linspace(8,8,20) 8];
hold on
plot(x,y, 'DisplayName', 'Lidar data')

scanned_angles = x;

%FIRST OBSTACLE
obstacle_average_distance = 0.5;
average_angle = 60;

center_of_obstacle = -50;

s = sigma_k(obstacle_average_distance, average_angle, VEICHLE_WIDTH);
l1 = likelihood_function(MAX_DETECTION_RANGE, obstacle_average_distance, s, center_of_obstacle, scanned_angles);

%SECOND OBSTACLE
obstacle_average_distance = 2.4;
average_angle = 30;

center_of_obstacle = 50;

s = sigma_k(obstacle_average_distance, average_angle, VEICHLE_WIDTH);
l2 = likelihood_function(MAX_DETECTION_RANGE, obstacle_average_distance, s, center_of_obstacle, scanned_angles);

%plot the repulsive field (sum of singular likelihood functions)
f_rep = l1+l2;
hold on
plot(x, f_rep, 'DisplayName', 'obstacles gaussian');

%ATTRACTIVE FIELD
gamma = 0.1; %arbitrary number
theta_g = -50; %value of goal's angle (arbitrary for now)

f_att = attractive_field(gamma, theta_g, scanned_angles);

plot(x, f_att, 'DisplayName', 'Attractive Field')

%plot the entire field, (attractive + repulsive fields)
f = f_rep + f_att;
plot(x,f, 'DisplayName', 'Total field');
%the local minimum will be the best point to follow
%print local minimum point
plot(x(f==min(f)), min(f), 'r*', 'DisplayName', 'Local minimum');
legend()